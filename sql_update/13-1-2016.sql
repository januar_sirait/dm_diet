ALTER TABLE `dm_diet`.`pasien`   
  ADD COLUMN `makanan_ingin` VARCHAR(250) NULL AFTER `tingkat_aktifitas`,
  ADD COLUMN `alergi` VARCHAR(250) NULL AFTER `makanan_ingin`;

ALTER TABLE `dm_diet`.`menu`  
  ADD FOREIGN KEY (`id_pasien`) REFERENCES `dm_diet`.`pasien`(`id_pasien`) ON UPDATE CASCADE ON DELETE CASCADE;