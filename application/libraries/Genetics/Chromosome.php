<?php 
class Genetics_Chromosome
{
	/**
	* Gen object
	* @var array
	*/
	public $_gen;

	/**
	* Fitness object
	*
	* @var float
	*/
	public $fitness;

	/**
	* kalori diet
	*
	* @var float
	*/
	public $kalori;

	/**
	* total kalori dari gen
	*
	* @var float
	*/
	public $total;

	function __construct()
	{
		$this->_gen = array();
	}

	public function getGen()
	{
		return $this->_gen;
	}

	public function getGenValue()
	{
		$gen = array();
		foreach ($this->_gen as $key => $item) {
			$gen[] = $item->_value;
		}

		return $gen;
	}

	public function setGet($gen)
	{
		$this->_gen = $gen;
	}
	
	
	//menghitung nilai fitness dengan rumus kalori pasien dikurang dengan total kalori gen 
	//nilai fitness yang terkecil merupakan nilai fitness yang terbaik dan akan digunakan
	public function countFitness()
	{
		$total_kalori = 0;
		foreach ($this->_gen as $gen) {
			$total_kalori += $gen->getValue();
		}

		$this->fitness = abs($this->kalori - $total_kalori);
		$this->total = $total_kalori;
	}
	
	//proses untuk mengambil nilai kandungan gizi
	public function getKandunganGizi()
	{
		$kandunganGizi = new stdClass();
		foreach ($this->_gen as $gen) {
			$kandunganGizi->protein = $gen->_protein;
			$kandunganGizi->lemak = $gen->_lemak;
			$kandunganGizi->carbohydrat = $gen->_carbohydrat;
			$kandunganGizi->sukrosa = $gen->_sukrosa;
			$kandunganGizi->natrium = $gen->_natrium;
			$kandunganGizi->kolesterol = $gen->_kolesterol;
			$kandunganGizi->serat = $gen->_serat;
		}

		return $kandunganGizi;
	}
}
?>