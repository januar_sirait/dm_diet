<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('GENETIC_ROOT')) {
    define('GENETIC_ROOT', dirname(__FILE__) . '/');
    require(GENETIC_ROOT . 'Genetics/Chromosome.php');
    require(GENETIC_ROOT . 'Genetics/Gen.php');
}

class Genetics
{

	/**
	 * Instance of this class
	 *
	 * @access	private
	 * @var Genetic
	 */
	private static $_instance;

	/**
	* Diet object
	*
	* @var stdClass
	*/	
	private $_diet;

	/**
	* Individu object
	*
	* @var array
	*/
	public $individu;

	/**
	* CI Object
	*
	* @var Object
	*/
	private $CI;

	/**
	* Bahan makanan object
	*
	* @var Object
	*/
	public $bahan_makanan;

	/**
	* object untuk menyimpan nilai random dari bahan makanan yang muncul
	*
	* @var Object
	*/
	public $list_random;

	/**
	* merupakan peluang mutasi yang digunakan untuk menghitung berapa banyak gen yang akan dimutasi
	*
	* @var float
	*/
	public $pm;

	/**
	* peluang cross over. Nilai yang digunakan untuk menghitung berapa banyak kromosom yang akan di crossover
	*
	* @var float
	*/
	public $cm;

	/**
	* array untuk menyimpan bahan makanan yang menjadi alergi bagi pasien
	*
	* @var array
	*/
	private $alergi;

	/**
	* array untuk menyimpan bahan makanan yang diinginkan
	*
	* @var array
	*/
	private $makanan_diinginkan;
	private $besar_populasi;

	public $log_list;
	public $log_item;

	const CARBOHYDRAT = 4;
	const PROTEIN = 4;
	const FAT = 9;

	const MAKANAN_POKOK = 'p';
	const LAUK_PAUK = 'l';
	const SAYUR = 's';
	const BUAH = 'b';
	const PELENGKAP = 'pl';

	public function __construct()
	{
		$this->individu = array();
		$this->CI =& get_instance();
		$this->CI->load->model('Makanan_model');

		$this->bahan_makanan = new stdClass();
		$this->bahan_makanan->{self::MAKANAN_POKOK} = null;
		$this->bahan_makanan->{self::LAUK_PAUK} = null;
		$this->bahan_makanan->{self::SAYUR} = null;
		$this->bahan_makanan->{self::BUAH} = null;
		$this->bahan_makanan->{self::PELENGKAP} = null;

		$this->list_random = new stdClass();
		$this->list_random->{self::MAKANAN_POKOK} = array();
		$this->list_random->{self::LAUK_PAUK} = array();
		$this->list_random->{self::SAYUR} = array();
		$this->list_random->{self::BUAH} = array();
		$this->list_random->{self::PELENGKAP} = array();

		$this->pm = 0.1;
		$this->cm = 0.5;

		$this->log_list = array();
		$this->log_item = new stdClass();
	}

	/*
	* set jenis diet user berdasarkan perhitungan kalori user 
	*/
	public function setDiet($diet)
	{
		$this->_diet = $diet;
	}

	/*
	* set bahan makanan yang menjadi alergi
	*/
	public function setAlergi($alergi)
	{
		$this->alergi = array();
		$this->alergi = explode(",",$alergi);
	}

	/*
	* set bahan makanan yang diiginkan oleh user
	*/
	public function setBahanMakananYgDiinginkan($bahan_makanan)
	{
		$this->makanan_diinginkan = array();
		$this->makanan_diinginkan = explode(",", $bahan_makanan);
	}
	/*proses ini akan melakukan generate populasi awal. 1 kromosom terdiri dari 15 individu
	  satu kromosom akan dibagi menjadi 3 bagian untuk makan pagi, siang, dan malam.
	  setiap sekali makan akan dibagi menjadi 5 untuk jenis bahan makanan yaitu pokok, lauk pauk, sayuran, buah, dan pelengkap.
	*/
	public function generateIndividu($total_individu)
	{
		$this->besar_populasi = $total_individu;
		$total_kalori = $this->_diet->energi / 3;

		for ($i=0; $i < $total_individu; $i++) { 
			$kromosom = new Genetics_Chromosome();
			$kromosom->kalori = $this->_diet->energi;
			for ($j=0; $j < 3; $j++) { 
				$makanan = $this->getBahanMakanan(self::MAKANAN_POKOK, $total_kalori);
				$kromosom->_gen[0 + ($j * 5)] = $makanan;
				$makanan = $this->getBahanMakanan(self::LAUK_PAUK, $total_kalori);
				$kromosom->_gen[1 + ($j * 5)] = $makanan;	
				$makanan = $this->getBahanMakanan(self::SAYUR, $total_kalori);
				$kromosom->_gen[2 + ($j * 5)] = $makanan;
				$makanan = $this->getBahanMakanan(self::BUAH, $total_kalori);
				$kromosom->_gen[3 + ($j * 5)] = $makanan;
				$makanan = $this->getBahanMakanan(self::PELENGKAP, $total_kalori);
				$kromosom->_gen[4 + ($j * 5)] = $makanan;
			}

			$kromosom->countFitness();
			$this->individu[] = $kromosom;
		}
		$this->sort();
	}

	/* private function */
	private function getBahanMakanan($type, $total_kalori)
	{
		$makanan = null;
		$tempKalori = 0;
		if ($type == self::MAKANAN_POKOK) {
			$tempKalori = (int) ($total_kalori * 0.45);
		}elseif ($type == self::LAUK_PAUK) {
			$tempKalori = (int) ($total_kalori * 0.20);
		}elseif ($type == self::SAYUR) {
			$tempKalori = (int) ($total_kalori * 0.15);
		}elseif ($type == self::BUAH) {
			$tempKalori = (int) ($total_kalori * 0.15);
		}elseif ($type == self::PELENGKAP) {
			$tempKalori = (int) ($total_kalori * 0.20);
		}

		$this->setBahanMakanan($type, $tempKalori);
		
		/* 
		code ini untuk menentukan index random untuk bahan makanan, jika bahan makanan sudah pernah diambil
		random dilakukan lagi.
		random yang dilakukan adalah random tidak berulang. Nilai random boleh berulang jika semua bahan 
		makanan sudah pernah digunakan
		*/
		$rand = rand(0, count($this->bahan_makanan->{$type}) - 1);
		if (count($this->bahan_makanan->{$type}) == count($this->list_random->{$type})) {
			$this->list_random->{$type} = array();
		}

		while (in_array($rand, $this->list_random->{$type})) {
			$rand = rand(0, count($this->bahan_makanan->{$type}) - 1);
		}

		// menyimpan bilangan unik hasil random
		$this->list_random->{$type}[] = $rand;
		/* end */

		$makanan = $this->bahan_makanan->{$type}[$rand];
		$gen = new Genetics_Gen($makanan->id, $makanan->value, $makanan->weight);
		$gen->setGen($makanan);

		return $gen;
	}

	/**
	 * Fungsi untuk normalisasi bahan makanan
	 * Function ini untuk mengambil bahan makanan sesuai tipe dari database
	 * kemudian dihitung berat bahan makanan, jika kalori makanan tersebut 
	 * melebihi dari kalori yang dibutuhkan maka kebutuhan berat makanan akan diubah.
	 *
	 * @var string $type
	 * @var int $tempKalori
	*/
	private function setBahanMakanan($type, $tempKalori)
	{
		if (!isset($this->bahan_makanan->{$type})) {

			$makanan = $this->CI->Makanan_model->getByType($type, 0, $this->alergi);

			$arrMakanan = array();
			foreach ($makanan as $item) {
				$data = new stdClass();
				$data->id = $item->code;
				if ($item->kj > $tempKalori) {
					$ratio = ($tempKalori / $item->kj);

					$data->value = $tempKalori;
					$data->weight = (int)($ratio * 100);
					$data->protein = ($ratio * $item->kd_protein);
					$data->lemak = ($ratio * $item->kd_fat);
					$data->carbohydrat = ($ratio * $item->kd_carbohydrat);
					$data->sukrosa = ($ratio * $item->kd_sucrose);
					$data->natrium = ($ratio * $item->kd_sodium);; //sodium
					$data->kolesterol = ($ratio * $item->kd_cholesterol);
					$data->serat = ($ratio * $item->kd_dietary_fiber); //dietary fiber
				}else{
					$data->value = $item->kj;
					$data->weight = 100;
					$data->protein = $item->kd_protein;
					$data->lemak = $item->kd_fat;
					$data->carbohydrat = $item->kd_carbohydrat;
					$data->sukrosa = $item->kd_sucrose;
					$data->natrium = $item->kd_sodium;
					$data->kolesterol = $item->kd_cholesterol;
					$data->serat = $item->kd_dietary_fiber;
				}
				$arrMakanan[] = $data;
			}
			$this->bahan_makanan->{$type} = $arrMakanan;
		}
	}
	
	/*
	melakukan cross over terhadap 2 individu terbaik yang memiliki nilai fitness terkecil
	posisi gen yang akan di cross over dirandom dengan panjang gen yang di cross over 3
	hasil akhir akan tercipta 2 generasi baru
	*/
	public function cross_over(){
		$jumlah_individu = (int)($this->cm * count($this->individu)); //proses untuk menghitung banyak jumlah individu yang akan di-crossover
		$jumlah_individu = ($jumlah_individu % 2 == 0) ? $jumlah_individu : $jumlah_individu+1;

		$this->log_item->crossover = new stdClass();
		$this->log_item->crossover->jumlah_individu = $jumlah_individu;
		$this->log_item->crossover->process = array();

		$index = 0;
		while ($index < $jumlah_individu) {
			$process = new stdClass();

			$individu = new Genetics_Chromosome(); 
			$individu->kalori = $this->individu[$index]->kalori;

			$index_awal = rand(0, count($this->individu[$index]->_gen)-1); //menentukan posisi indeks awal secara random
			$index_akhir = rand(0, count($this->individu[$index]->_gen)-1); //menentukan posisi indeks akhir secara random
			
			//jika saat random indeks awal=indeks akhir maka random kembali indeks akhir
			while ($index_awal == $index_akhir) {
				$index_akhir = rand(0, count($this->individu[$index]->_gen)-1);
			}

			$process->index_awal = $index_awal;
			$process->index_akhir = $index_akhir;

			// proses untuk melakukan proses crossover dengan metode two point crossover
			for ($i=0; $i < count($this->individu[$index]->_gen); $i++) { 
				if ($index_akhir < $index_awal) {
					if ($i <= $index_akhir || $i >= $index_awal) {
						$individu->_gen[$i] = $this->individu[$index + 1]->_gen[$i]; //mencetak individu kedua
					}else{
						$individu->_gen[$i] = $this->individu[$index]->_gen[$i]; //mencetak individu pertama
					}
				}
				
				else{
					if ($i >= $index_awal && $i <= $index_akhir) {
						$individu->_gen[$i] = $this->individu[$index + 1]->_gen[$i]; //mencetak individu kedua
					}else{
						$individu->_gen[$i] = $this->individu[$index]->_gen[$i]; //mencetak individu pertama
					}
				}
			}

			$individu->countFitness();
			$this->individu[] = $individu;
			$process->parent1 = $this->individu[$index]->getGenValue();
			$process->parent2 = $this->individu[$index + 1]->getGenValue();
			$process->child = $individu->getGenValue();
			$this->log_item->crossover->process[] = $process;
			$index += 2;
		}
	}

	/*
	proses ini dilakukan untuk mutasi antar gen dalam satu kromosom
	*/
	public function mutation()
	{
		$this->log_item->mutation = new stdClass();

		$total_mutation_gen = (int)($this->pm * count($this->individu) * 15);
		$this->log_item->mutation->jumlah_gen = $total_mutation_gen;
		$this->log_item->mutation->process = array();
		for ($i=0; $i < $total_mutation_gen; $i++) { 
			$process = new stdClass();
			$randGen = rand(0, 14);
			$randKrom = rand(0, count($this->individu) - 1);
			$swapIndexGen = $randGen + 5;

			if ($randGen >= 10) {
				$swapIndexGen = $randGen % 5;
			}

			$process->posisi_gen = $randGen;
			$process->posisi_individu = $randKrom;
			$process->individu_awal = $this->individu[$randKrom]->getGenValue();

			$genTemp = $this->individu[$randKrom]->_gen[$randGen];
			$this->individu[$randKrom]->_gen[$randGen] = $this->individu[$randKrom]->_gen[$swapIndexGen];
			$this->individu[$randKrom]->_gen[$swapIndexGen] = $genTemp;

			$process->individu_akhir = $this->individu[$randKrom]->getGenValue();

			$this->log_item->mutation->process[] = $process;
		}
	}

	/*
	* fungsi untuk menghitung fitness dari setiap individu
	* fungsi untuk menghitung fitness sendiri tersedia dalam
	* object Chromosome yang merupakan class untuk membentuk individu
	*/
	public function countAllFitness()
	{
		foreach ($this->individu as $individu) {
			$individu->countFitness();
		}
	}

	public function getIndividu()
	{
		$populasi = array();
		foreach ($this->individu as $key => $item) {
			$individu = new stdClass();
			$individu->fitness = $item->fitness;
			$individu->gen = $item->getGenValue();
			$populasi[] = $individu;
		}

		return $populasi;

	}

	public function getAvgFitness()
	{
		$temp = 0;
		for ($i=0; $i < 7; $i++) { 
			$temp += $this->individu[$i]->fitness;
		}

		return round($temp / 7, 2);
	}

	/*
	* sort individu berdasarkan fitness. fitness terbaik berada pada urutan paling atas
	* ASC
	*/
	public function sort()
	{
		for ($i=0; $i < count($this->individu) - 1; $i++) { 
			$smallest = $this->individu[$i]->fitness;
			$index = $i;
			for ($j=$i; $j < count($this->individu) ; $j++) { 
				if ($this->individu[$j]->fitness < $smallest) {
					$index = $j;
					$smallest = $this->individu[$j]->fitness;
				}
			}
			
			//swap
			$temp = $this->individu[$i];
			$this->individu[$i] = $this->individu[$index];
			$this->individu[$index] = $temp;
		}

		$individu = array();
		for ($i=0; $i < $this->besar_populasi; $i++) { 
			$individu[] = clone $this->individu[$i];
		}
		$this->individu = $individu;
	}
}
?>