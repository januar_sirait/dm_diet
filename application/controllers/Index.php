<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* @author : Vanesa
* @copyright : 2015
* 
*/
class Index extends MY_Controller
{
	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$this->view(null, false);
	}

	public function preprocess()
	{
		$scripts = array();
		$scripts[] = 'jquery.validate.js';
		$scripts[] = 'validate.js';
		$scripts[] = 'scripts.js';
        $this->data['footer']['scripts'] = $scripts;
		$this->view();
	}
}
?>