<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* @author : Vanesa
* @copyright : 2015
*/
class Genetic extends MY_Controller
{
	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$scripts = array();
		$scripts[] = 'jquery.validate.js';
		$scripts[] = 'validate.js';
		$scripts[] = 'genetic.js';
        $this->data['footer']['scripts'] = $scripts;
		$this->view();
	}

	public function count_weight()
	{
		$response = new stdClass();
		$response->status = true;
		$response->data = new stdClass();
		$response->data->user = $this->request;
		$response->data->bbi = $this->hitungBeratBadanIdeal($this->request->tinggi_badan, $this->request->jenis_kelamin);
		$response->data->kb = $this->hitungKaloriBasal($this->request->jenis_kelamin, $response->data->bbi);
		$response->data->kfa = $this->hitungKoreksiFaktorAktifitas($this->request->tingkat_aktifitas, $response->data->kb);
		$response->data->kfu = $this->hitungKoreksiFaktorUsia($this->request->usia, $response->data->kb);
		$response->data->imt = $this->hitungIndexMasaTubuh($this->request->berat_badan, $this->request->tinggi_badan);
		$response->data->total_kalori = $response->data->kb + $response->data->kfa + $response->data->kfu + $response->data->imt['kalori'];

		$this->load->model('Jenisdiet_model');
		$diet_max = $this->Jenisdiet_model->getByKalori($response->data->total_kalori);
		$diet_min = $this->Jenisdiet_model->getByKalori($response->data->total_kalori, '<=', 'DESC');
		$response->data->jenis_diet = (abs($diet_max->energi - $response->data->total_kalori) <= abs($diet_min->energi - $response->data->total_kalori)) ? $diet_max : $diet_min;

		$this->session->set_userdata('kalori', $response->data);

		$this->json($response);
	}

	public function process()
	{
		ini_set('memory_limit', '-1');
		$this->load->library('genetics');
		$this->load->model('Makanan_model');
		$individu_awal = $this->input->post('individu_awal');
		$individu_awal = isset($individu_awal) ? $individu_awal : 10;
		$generasi = $this->input->post('generasi');
		$generasi = isset($generasi)? $generasi : 10;
		$grafik = array();

		$kalori = $this->session->kalori;
		$genetic = new Genetics();
		$genetic->setDiet($kalori->jenis_diet);
		$genetic->setAlergi($kalori->user->alergi);
		$genetic->setBahanMakananYgDiinginkan($kalori->user->makanan_ingin);
		$genetic->generateIndividu($individu_awal);
		$genetic->log_item->populasi = $genetic->getIndividu();
		$debug['generate'] = $genetic->individu;
		for ($i=0; $i < $generasi; $i++) { 
			$grafik[] = array($i+1, $genetic->getAvgFitness());
			$genetic->cross_over();
			$genetic->mutation();
			$genetic->sort();

			$genetic->log_list[] = $genetic->log_item;
			$genetic->log_item = new stdClass();
			$genetic->log_item->populasi = $genetic->getIndividu();
		}
		$debug['last_population'] = $genetic->individu;
		//$this->json($genetic);
		
		$result = array(
			'status' => true,
			'data'	 => array(),
			'makanan'=> null,
			'grafik' => (object)array('populasi'=> $individu_awal, 'data'=> $grafik)
		);
		for ($i=0; $i < 7; $i++) { 
			$menu = array();
			foreach ($genetic->individu[$i]->_gen as $gen) {
				$makanan = (array)$this->Makanan_model->getByCode($gen->_id);
				$makanan['kalori'] = $gen->_value;
				$makanan['weight'] = $gen->_weight;
				$menu[] = $makanan;
			}
			$result['data'][$i] = new stdClass();
			$result['data'][$i]->menu = $menu;
			$result['data'][$i]->total_kalori = $genetic->individu[$i]->total;
			$result['data'][$i]->kandungan_gizi = $genetic->individu[$i]->getKandunganGizi();
		}

		$result['makanan'] = $genetic->bahan_makanan;
		$result['individu'] = $genetic->individu;
		$this->session->set_userdata('menu', $result['data']);
		$this->session->set_userdata('grafik', $result['grafik']);

		file_put_contents(FCPATH . "assets/log.json", json_encode($genetic->log_list));
		$this->json($result);
	}

	public function save()
	{
		//print_r($this->session->kalori);
		$this->load->model('Menu_model');
		$this->load->model('Pasien_model');
		$user = (array)$this->session->kalori->user;

		$id_pasien = $this->Pasien_model->insert($user);
		$menu = $this->session->menu;
		for ($i=1; $i <= count($menu); $i++) { 
			foreach ($menu[$i - 1]->menu as $item) {
				$data = array(
					'id_pasien' => $id_pasien,
					'code'		=> $item['code'],
					'kalori'	=> $item['kalori'],
					'weight'	=> $item['weight'],
					'hari'		=> $i
				);
				$this->Menu_model->insert($data);
			}
		}

		redirect('genetic/show_data');
	}

	public function show_data()
	{
		$scripts = array();
		$scripts[] = 'show_data.js';
		$this->data['footer']['scripts'] = $scripts;

		$this->load->model('Pasien_model');
		$listPasien = $this->Pasien_model->getAll();
		$this->data['view']['listPasien'] = $listPasien;
		$this->view();
	}

	public function detail($id)
	{
		$this->load->model('Pasien_model');
		$this->load->model('Menu_model');

		$this->data['view']['pasien'] = $this->Pasien_model->getByPasien($id);

		$menu = array();
		for ($i=1; $i <= 7; $i++) { 
			$menu_sehari = array();
			$data = $this->Menu_model->getByPasien($id, $i);
			foreach ($data as $item) {
				$menu_sehari[] = $item;
			}
			$menu[] = $menu_sehari;
		}
		$this->data['view']['menu'] = $menu;
		$this->view();
	}

	public function show_grafik()
	{
		$scripts = array();
		$scripts[] = 'jqplot/jquery.jqplot.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.json2.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.barRenderer.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.categoryAxisRenderer.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.pointLabels.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.canvasTextRenderer.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js';
        $scripts[] = 'jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js';
		$scripts[] = 'show_grafik.js';
        $this->data['footer']['scripts'] = $scripts;

        $style[] = array(
            'assets' => TRUE,
            'href' => 'javascripts/jqplot/css/jquery.jqplot.min.css');
        $this->data['header']['style'] = $style;

        if ($this->isPost()) {
        	$grafik = $this->session->grafik;
        	$this->json($grafik);
        }

		$this->view();
	}

	public function show_log($id)
	{
		ini_set('memory_limit', '-1');
		if (file_exists(FCPATH . "assets/log.json")) {
			$content = file_get_contents(FCPATH . "assets/log.json");

			$scripts = array();
			$scripts[] = 'paging.js';
			$this->data['footer']['scripts'] = $scripts;

			$logs = json_decode($content);
			// $this->json($log);
			$log = $logs[$id-1];
			$this->data['view']['current'] = $id;
			$this->data['view']['total'] = count($logs);
			$this->data['view']['log'] = $log;
			$this->view();
		}
	}

	public function delete($id)
	{
		$this->load->model('Pasien_model');
		$this->Pasien_model->delete($id);

		$this->json(array('status' => true));
	}

	/* private function */
	private function hitungBeratBadanIdeal($tinggi_badan, $jenis_kelamin)
	{
		if ($jenis_kelamin == "laki-laki") {
			if ($tinggi_badan < 160) {
				return $tinggi_badan - 100;
			} else {
				return ($tinggi_badan - 100) * 0.9;
			}
		}else{
			if ($tinggi_badan < 150) {
				return $tinggi_badan - 100;
			}else{
				return ($tinggi_badan - 100) * 0.9;
			}
		}
	}

	private function hitungKaloriBasal($jenis_kelamin, $bbi)
	{
		if ($jenis_kelamin == "laki-laki") {
			return $bbi * 30;
		} else {
			return $bbi * 25;
		}
	}

	private function hitungKoreksiFaktorAktifitas($tingkat_aktifitas, $kb)
	{
		return ($tingkat_aktifitas == 'ringan') ? $kb * 0.1 :
			(($tingkat_aktifitas == 'sedang') ? $kb * 0.3 : 
				(($tingkat_aktifitas == "berat") ? $kb * 0.4 : 0));
	}

	private function hitungKoreksiFaktorUsia($usia, $kb)
	{
		$koreksi = 0;

		if ($usia >= 70) {
			$koreksi = $kb * 0.2;
		}else if ($usia >= 60) {
			$koreksi = $kb * 10;
		}else if ($usia >= 40) {
			$koreksi = $kb * 0.05;
		}

		return (-1 * $koreksi);
	}

	private function hitungIndexMasaTubuh($berat_badan, $tinggi_badan)
	{
		$tinggi_badan = $tinggi_badan * 0.01;
		$imt = ($berat_badan/(pow($tinggi_badan, 2)));
		$result = array(
			'value' => $imt,
			'name'	=> 'normal',
			'kalori'=> 0	
		);

		if ($imt >= 28) {
			$result['name'] = 'obesitas';
			$result['kalori'] = -700;
		}else if ($imt > 25) {
			$result['name'] = 'kegemukan';
			$result['kalori'] = -500;
		}else if ($imt < 18) {
			$result['name'] = 'kurus';
			$result['kalori'] = 500;
		}

		return $result;
	}
}
?>