<?php 
class Menu_model extends MY_Model
{
	private $_table = 'menu';

	function __construct()
	{
		parent::__construct($this->_table);
	}

	public function getByKalori($kalori)
	{
		$this->db->where('energi >=', $kalori);
		$this->db->order_by('energi', 'ASC');
		$query = $this->db->get($this->_table);
		foreach ($query->result() as $row) {
			return $row;
		}
		return null;
	}

	public function getAll()
	{
		$query = $this->db->get($this->_table);
		return $query->result();
	}

	public function getByPasien($id_pasien, $hari)
	{
		$this->db->select('menu.*, tabel_makanan.nama_makanan');
		$this->db->join('tabel_makanan', $this->_table . '.code = tabel_makanan.code');
		$this->db->where('id_pasien =', $id_pasien);
		$this->db->where('hari =', $hari);
		$this->db->order_by('id_menu', 'ASC');
		$query = $this->db->get($this->_table);

		return $query->result();
	}

	public function insert($data) {
        $data['id_menu'] = null;
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }
}
?>