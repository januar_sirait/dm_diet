<?php 
class Pasien_model extends MY_Model
{
	private $_table = 'pasien';

	function __construct()
	{
		parent::__construct($this->_table);
	}

	public function getAll()
	{
		$query = $this->db->get($this->_table);
		return $query->result();
	}

	public function getByPasien($id_pasien)
	{
		$this->db->where('id_pasien =', $id_pasien);
		$query = $this->db->get($this->_table);

		foreach ($query->result() as $row) {
			return $row;
		}
		return null;
	}

	public function insert($data) {
        $data['id_pasien'] = null;
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    public function delete($id) {
        $this->db->where('id_pasien', $id);
        $this->db->delete($this->_table);
        return $this->db->affected_rows();
    }
}
?>