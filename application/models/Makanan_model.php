<?php 
class Makanan_model extends MY_Model
{
	private $_table = 'tabel_makanan';

	function __construct()
	{
		parent::__construct($this->_table);
	}

	public function getByKalori($kalori)
	{
		$this->db->where('energi >=', $kalori);
		$this->db->order_by('energi', 'ASC');
		$query = $this->db->get($this->_table);
		foreach ($query->result() as $row) {
			return $row;
		}
		return null;
	}

	public function getByType($type, $max_kalori = 0, $alergi = array())
	{
		$this->db->where('tipe_makanan =', $type);
		if ($max_kalori > 0) {
			$this->db->where('kj <=', $max_kalori);
		}

		$this->db->where('kj >', 0);

		foreach ($alergi as $item) {
			if (isset($item) && $item != '') {
				$this->db->not_like('nama_makanan', $item);
			}
		}

		$query = $this->db->get($this->_table);

		return $query->result();
	}

	public function getByCode($code)
	{
		$this->db->select('*, "" as kalori, "" as weight');
		$this->db->where('code =', $code);
		$query = $this->db->get($this->_table);

		foreach ($query->result() as $row) {
			return $row;
		}
		return null;
	}
}
?>