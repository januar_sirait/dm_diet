<?php 
class Jenisdiet_model extends MY_Model
{
	private $_table = 'jenis_diet';

	function __construct()
	{
		parent::__construct($this->_table);
	}

	public function getByKalori($kalori, $operator = '>=', $order = 'ASC')
	{
		$this->db->where('energi '. $operator, $kalori);
		$this->db->order_by('energi', $order);
		$query = $this->db->get($this->_table);
		foreach ($query->result() as $row) {
			return $row;
		}
		return null;
	}
}
?>