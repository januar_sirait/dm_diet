	<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h5>Developed by Vanesa Felicia</h5>
				</div>
				<div class="col-lg-12">
					<span class="copyright">© 2016-Vanesa. All Rights Reserved.</span>
				</div>
			</div>
		</div>
	</footer>


	<!-- jQuery -->
    <script src="<?php echo(asset_url_script('jquery.js')) ?>"></script>
    <script src="<?php echo(asset_url_script('jquery-ui/jquery.effects.core.js')) ?>"></script>
    <script src="<?php echo(asset_url_script('jquery-ui/jquery.effects.slide.js')) ?>"></script>
    <script src="<?php echo(asset_url_script('jquery-slide.js')) ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo(asset_url_script('bootstrap.min.js')) ?>"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo(asset_url_script('jquery.easing.min.js')) ?>"></script>
    <script src="<?php echo(asset_url_script('jquery.fittext.js')) ?>"></script>
    <script src="<?php echo(asset_url_script('wow.min.js')) ?>"></script>
    <?php echo(script_tag('steps/js/jquery.steps.min.js', TRUE)) ?>
	
    <!-- Custom Theme JavaScript -->
    <!--<script src="<?php echo(asset_url_script('creative.js')) ?>"></script>-->
	<?php
	if (isset($scripts)) {
	    foreach ($scripts as $value) {
	        if (is_array($value)) {
	            if(isset($value['assets']) && $value['assets'] == TRUE)
	            {
	                echo(script_tag($value['src'], TRUE));
	            }else{
	                echo(script_tag($value['src']));
	            }
	        }else{
	            echo(script_tag($value));
	        }
	    }
	}
	?>

	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('[data-toggle="tooltip"]').tooltip();
	});
	</script>
</body>

</html>