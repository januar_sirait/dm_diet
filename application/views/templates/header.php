<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo($title) ?></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo(asset_url_css('bootstrap.min.css')) ?>" type="text/css">

    <!-- Custom Fonts -->
    <link href='<?php echo(asset_url_css('font/OpenSans.css')) ?>' rel='stylesheet' type='text/css'>
    <link href='<?php echo(asset_url_css('font/Merriweather.css')) ?>' rel='stylesheet' type='text/css'>
    <?php echo(link_tag('font-awesome/css/font-awesome.min.css', 'stylesheet', 'text/css', '', '', TRUE)) ?>

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo(asset_url_css('animate.min.css')) ?>" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo(asset_url_css('creative.css')) ?>" type="text/css">
    <?php echo(link_tag('steps/css/jquery-steps.min.css', 'stylesheet', 'text/css', '', '', TRUE)) ?>

    <!-- Overwrite style -->
    <link rel="stylesheet" type="text/css" href="<?php echo(asset_url_css('style.css')) ?>">
    <?php
        if (isset($style)) {
            foreach ($style as $value) {
                if (is_array($value)) {
                    if(isset($value['assets']) && $value['assets'] == true){
                        echo(link_tag($value['href'],'stylesheet', 'text/css','','',TRUE));
                    }else{
                        echo(link_tag($value['href']));
                    }
                }  else {
                    echo(link_tag($value));
                }
            }
        }
    ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">
    
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>DM Diet</h1>
                    <h4>App to schedule the diet of diabetes mellitus patients</h4>
                </div>
            </div>   
        </div>

        <nav id="mainNav" class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand page-scroll" href="#page-top">Start Bootstrap</a> -->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="page-scroll" href="<?php echo(base_url()) ?>#page-top">Beranda</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="<?php echo(base_url('genetic'))?>">Penjadwalan Makanan</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="<?php echo(base_url('genetic/show_data'))?>">Data Pasien</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="<?php echo(base_url()) ?>#about">Apa itu Diabetes Mellitus?</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
