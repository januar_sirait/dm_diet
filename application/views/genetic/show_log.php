<div class="container content blog">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">
				<center>Proses Algoritma Genetika<hr>
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-12">
				<div class="log-block">
						<div class="log-header">
							Generasi ke :  <?php echo($current) ?>
						</div>
						<div class="log-body">
							<label>Populasi</label>
							<div class="row">
								<?php for ($i=0; $i < count($log->populasi); $i++) {
								?>
								<div class="col-md-7">
									<span><?php echo(sprintf("Individu %d, <br> Fitness : %d", $i+1, $log->populasi[$i]->fitness))?></span>
									<table class="table table-bordered table-individu">
										<tr>
											<?php 
											foreach ($log->populasi[$i]->gen as $gen) {
											?>
											<td><?php echo($gen) ?></td>
											<?php
											}
											?>
										</tr>
									</table>
								</div>
								<?php
								} ?>
							</div>	

							<label>Crossover</label> <br/>
							<span>Jumlah Individu : <?php echo($log->crossover->jumlah_individu) ?></span>
							<div class="row">
							<?php
								for ($i=0; $i < count($log->crossover->process); $i++) { 
							?>
								<div class="col-md-7">
									<span><?php echo(sprintf("Proses %d", $i+1)) ?></span> <br/>
									<span><?php echo(sprintf("Index awal %d, index akhir %d", $log->crossover->process[$i]->index_awal, $log->crossover->process[$i]->index_akhir))?></span>
									<br/>
									<span>Parent 1</span>
									<table class="table table-bordered table-individu">
										<tr>
											<?php 
											foreach ($log->crossover->process[$i]->parent1 as $gen) {
											?>
											<td><?php echo($gen) ?></td>
											<?php
											}
											?>
										</tr>
									</table>
									<span>Parent 2</span>
									<table class="table table-bordered table-individu">
										<tr>
											<?php 
											foreach ($log->crossover->process[$i]->parent2 as $gen) {
											?>
											<td><?php echo($gen) ?></td>
											<?php
											}
											?>
										</tr>
									</table>

									<span>Offspring</span>
									<table class="table table-bordered table-individu">
										<tr>
											<?php 
											foreach ($log->crossover->process[$i]->child as $gen) {
											?>
											<td><?php echo($gen) ?></td>
											<?php
											}
											?>
										</tr>
									</table>
								</div>
							<?php
							} 
							?>
								
							</div>

							<label>Mutasi</label><br/>
							<span>Jumlah Gen : <?php echo($log->mutation->jumlah_gen) ?></span>
							<div class="row">
								<div class="col-md-7">
									<?php for ($i=0; $i < count($log->mutation->process); $i++) { 
									?>
									<span><?php echo(sprintf("Proses %d", $i+1)) ?></span> <br/>
									<span><?php echo(sprintf("Index individu : %d, Index gen : %d", $log->mutation->process[$i]->posisi_individu, $log->mutation->process[$i]->posisi_gen)) ?></span>
									<br/>
									<span>Individu awal</span>
									<table class="table table-bordered table-individu">
										<tr>
											<?php 
											foreach ($log->mutation->process[$i]->individu_awal as $gen) {
											?>
											<td><?php echo($gen) ?></td>
											<?php
											}
											?>
										</tr>
									</table>
									<span>Individu akhir</span>
									<table class="table table-bordered table-individu">
										<tr>
											<?php 
											foreach ($log->mutation->process[$i]->individu_akhir as $gen) {
											?>
											<td><?php echo($gen) ?></td>
											<?php
											}
											?>
										</tr>
									</table>
									<?php
									} ?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Pagination -->
						<nav style="text-align: center;">
							<ul class="pagination">
								<li class="<?php echo($current == 1 ? 'disabled':'') ?>">
									<a href="/genetic/show_log/1">
										<span>First</span>
									</a>
								</li>
								<li class="<?php echo($current == 1 ? 'disabled':'') ?>">
									<a href="/genetic/show_log/<?php echo($current - 1) ?>" aria-label="Previous">
										<span aria-hidden="true">&laquo;</span>
									</a>
								</li>
								<?php
								if ($total <= 5) {
									for ($i=1; $i <= $total; $i++) { 
								?>
								<li class="<?php echo($i == $current ? 'active':'') ?>">
									<a href="/genetic/show_log/<?php echo($i) ?>"><?php echo($i) ?></a>
								</li>
								<?php
								 	}
								}elseif ($current <= 3 && $total > 5) {
								 	for ($i=1; $i <= 5; $i++) { 
								?>
								<li class="<?php echo($i == $current ? 'active':'') ?>">
									<a href="/genetic/show_log/<?php echo($i) ?>"><?php echo($i) ?></a>
								</li>
								<?php
								 	}
								}elseif ($current > 3 && ($total - $current > 3)) {
									for ($i=$current-2; $i <= $current + 2; $i++) { 
								?>
								<li class="<?php echo($i == $current ? 'active':'') ?>">
									<a href="/genetic/show_log/<?php echo($i) ?>"><?php echo($i) ?></a>
								</li>
								<?php
									}
								}elseif ($total - $current <= 3) {
									for ($i=$total-4; $i <= $total; $i++) { 
								?>
								<li class="<?php echo($i == $current ? 'active':'') ?>">
									<a href="/genetic/show_log/<?php echo($i) ?>"><?php echo($i) ?></a>
								</li>
								<?php
									}
								} 
								?>
								<li class="<?php echo($current == $total ? 'disabled':'') ?>">
									<a href="/genetic/show_log/<?php echo($current + 1) ?>" aria-label="Next">
										<span aria-hidden="true">&raquo;</span>
									</a>
								</li>
								<li class="<?php echo($current == $total ? 'disabled':'') ?>">
									<a href="/genetic/show_log/<?php echo($total) ?>">
										<span>Last</span>
									</a>
								</li>
							</ul>
						</nav>
			</div>
		</div>
	</div>
</div>