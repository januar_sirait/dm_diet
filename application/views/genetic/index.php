<div class="container content">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">
				Penjadwalan Menu Makanan
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="form-wizard clearfix">
			<div class="steps clearfix">
				<ul role="tablist">
					<li role="tab" class="first current" aria-disabled="false" aria-selected="true">
						<a id="wizard-t-0" href="#wizard-h-0" aria-controls="wizard-p-0">
							<span class="current-info audible">current step: </span>
							<span class="number">1.</span>
							<span class="title">Data Pasien</span>
						</a>
					</li>
					<li role="tab" class="disabled" aria-disabled="true">
						<a id="wizard-t-1" href="#wizard-h-1" aria-controls="wizard-p-1">
							<span class="number">2.</span>
							<span class="title">Data Diet Pasien</span>
						</a>
					</li>
					<li role="tab" class="disabled last" aria-disabled="true">
						<a id="wizard-t-2" href="#wizard-h-2" aria-controls="wizard-p-2">
							<span class="number">3.</span>
							<span class="title">Jadwal Menu Makanan</span>
						</a>
					</li>
				</ul>

				<div class="content step-body clearfix">

					<form id="form-step-1" class="form-horizontal form-bordered form-step" data-current="true" data-list="#wizard-t-0" data-finished="false">
						<div class="wizard-container">
							<div class="form-group">
								<div class="col-md-12">
									<h5 class="semibold text-primary nm">*Data pribadi penderita diabetes melitus.</h5>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<input class="form-control" name="nama" id="nama" required></input>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Kelamin</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-3">
											<select class="form-control" name="jenis_kelamin" required>
												<option value="">Please choose</option>
												<option value="laki-laki">Laki-laki</option>
												<option value="perempuan">Perempuan</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Usia</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-2">
											<input class="form-control required" name="usia" id="usia"></input>
										</div>
										<label class="control-label">tahun</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Berat Badan</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-2">
											<input class="form-control required digits" name="berat_badan" id="berat_badan"></input>
										</div>
										<label class="control-label">kg</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tinggi Badan</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-2">
											<input class="form-control required digits" name="tinggi_badan" id="tinggi_badan"></input>
										</div>
										<label class="control-label">cm</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tingkat Aktivitas</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-3">
											<select class="form-control required" name="tingkat_aktifitas" data-toggle="tooltip" data-placement="top" title="<ol><li>Aktivitas ringan seperti mengajar, membaca, berjalan, bekerja di kantor, dan memancing</li>
											<li>Aktivitas sedang seperti bersepeda, bowling, berkebun, dan membersihkan rumah</li>
											<li>Aktivitas berat seperti olahraga aerobic, jogging, menari, bersepeda cepat, atlit.</li>
											</ol>" data-html="true">
												<option value="">Please choose</option>
												<option value="ringan">ringan</option>
												<option value="sedang">sedang</option>
												<option value="berat">berat</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alergi</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<input class="form-control" name="alergi" id="alergi" data-toggle="tooltip" data-placement="top" title="gunakan tanda koma(,) sebagai pemisah jika bahan makanan lebih dari satu item"/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" style="display:none">
								<label class="col-sm-2 control-label">Makan yang diinginkan</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<input class="form-control" name="makanan_ingin" id="makanan_ingin" data-toggle="tooltip" data-placement="top" title="gunakan tanda koma(,) sebagai pemisah jika bahan makanan lebih dari satu item"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>

					<form id="form-step-2" class="form-horizontal form-bordered form-step form-hide" data-current="false" data-list="#wizard-t-1" data-finished="false">
						<div class="wizard-container">
							<!-- <div class="form-group">
								<div class="col-md-12">
									<h5 class="semibold text-primary nm">Provide some of your details.</h5>
									<p class="text-muted nm">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div> -->
							<div class="bs-callout bs-callout-info">
								<h4>Data Pasien</h4>
								<div class="form-group">
									<label class="col-sm-2 control-label">Nama</label>
									<div class="col-sm-4">
										<label class="control-label" id="nama"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Jenis Kelamin</label>
									<div class="col-sm-5">
										<label class="control-label" id="jenis_kelamin"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Usia</label>
									<div class="col-sm-5">
										<label class="control-label" id="usia"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Berat Badan</label>
									<div class="col-sm-5">
										<label class="control-label" id="berat_badan"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tinggi Badan</label>
									<div class="col-sm-5">
										<label class="control-label" id="tinggi_badan"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Tingkat Aktivitas</label>
									<div class="col-sm-5">
										<label class="control-label" id="tingkat_aktifitas"></label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Alergi</label>
									<div class="col-sm-5">
										<label class="control-label" id="alergi"></label>
									</div>
								</div>
								<!--<div class="form-group" style="display:none">
									<label class="col-sm-2 control-label">Makan yang diinginkan</label>
									<div class="col-sm-5">
										<label class="control-label" id="makanan_ingin"></label>
									</div>
								</div>-->
							</div>

							<div class="bs-callout bs-callout-info">
								<h4>Berat Badan dan Kebutuhan Kalori</h4>
								<div class="form-group">
									<label class="col-sm-2 control-label">Berat Badan Ideal</label>
									<div class="col-sm-5">
										<label class="control-label" id="bbi">
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Kalori Basal</label>
									<div class="col-sm-5">
										<label class="control-label" id="kb">
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Koreksi Faktor Aktifitas</label>
									<div class="col-sm-5">
										<label class="control-label" id="kfa">
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Koreksi Faktor Usia</label>
									<div class="col-sm-5">
										<label class="control-label" id="kfu">
										</label>
									</div>
								</div>
								<!--<div class="form-group">
									<label class="col-sm-2 control-label">Index Masa Tubuh</label>
									<div class="col-sm-5">
										<label class="control-label" id="imt">
										</label>
									</div>
								</div>-->
								<div class="form-group">
									<label class="col-sm-2 control-label">Total Kebutuhan Kalori</label>
									<div class="col-sm-5">
										<label class="control-label" id="total_kalori">
										</label>
									</div>
								</div>
								<table id="table-kalori" class="table table-bordered">
									<thead>
										<tr>
											<th>Jenis diet</th>
											<th>Energi</th>
											<th>Protein</th>
											<th>Lemak</th>
											<th>Karbohidrat</th>
											<th>Kolesterol</th>
											<th>Serat</th>
											<th>Natrium</th>
											<th>Sukrosa</th>
										</tr>
									</thead>
								</table>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">Jumlah Individu Awal</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<select class="form-control" name="individu_awal" id="individu_awal" required>
												<option value="10">10</option>
												<option value="20">20</option>
												<option value="30">30</option>
												<option value="40">40</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jumlah Generasi</label>
								<div class="col-sm-10">
									<div class="row">
										<div class="col-sm-5">
											<select class="form-control" name="generasi" id="generasi" required>
												<option value="10">10</option>
												<option value="20">20</option>
												<option value="30">30</option>
												<option value="40">40</option>
												<option value="50">50</option>
												<option value="60">60</option>
												<option value="70">70</option>
												<option value="80">80</option>
												<option value="90">90</option>
												<option value="100">100</option>
												<option value="110">110</option>
												<option value="120">120</option>
												<option value="150">150</option>
												<option value="200">200</option>
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-12 loading">
								<img src="<?php echo(asset_url_images('progressbar.gif')) ?>">
							</div>
						</div>
					</form>

					<form id="form-step-3" class="form-horizontal form-bordered form-step form-hide" data-current="false" data-list="#wizard-t-2" data-finished="false">
						<div class="wizard-container">
							<div id="schedule" style="overflow-y: auto;">
								<a class="btn btn-primary" href="/genetic/show_grafik" target="_blank"><i class="fa fa-line-chart"></i> Lihat Grafik</a>
								<a class="btn btn-primary" href="/genetic/show_log/1" target="_blank"><i class="fa fa-bars"></i> Log</a>

								<div class="schedule-box col-lg-12 col-xs-12">
									<div class="header"><i class="fa fa-calendar"></i> {day}</div>
									<div class="body">
										<label>Total Kalori : {kalori}</label>
										<table class="table table-bordered table-header">
										<thead>
											<tr>
												<th>Tipe Makanan</th>
												<th>Sarapan</th>
												<th>Makan Siang</th>
												<th>Makan Malam</th>
											</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="actions clearfix">
				<ul role="menu" aria-label="Pagination">
					<li class="disabled" aria-disabled="true">
						<a id="previous" href="#previous" role="menuitem" class="btn btn-default">Previous</a>
					</li>
					<li aria-hidden="false" aria-disabled="false">
						<a id="next" href="#next" role="menuitem" class="btn btn-default">Next</a>
					</li>
					<li aria-hidden="true">
						<a id="finish" href="#finish" role="menuitem" class="btn btn-primary" style="display:none">Finish</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>