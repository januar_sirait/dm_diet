<!--halaman untuk menampilkan data pasien diabetes mellitus-->
<div class="container content">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">
				Data Pasien
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table id="table-diet" class="table table-bordered">
				<thead>
					<tr class="active">
						<th>No.</th>
						<th>Nama</th>
						<th>Jenis Kelamin</th>
						<th>Usia</th>
						<th>Berat Badan</th>
						<th>Tinggi Badan</th>
						<th>Tingkat Aktifitas</th>
						<th>Alergi</th>
						<th style="display:none">Makanan yang diinginkan</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1; 
					foreach ($listPasien as $pasien) {
					?>
					<tr>
						<td><?php echo($i) ?></td>
						<td><?php echo($pasien->nama) ?></td>
						<td><?php echo($pasien->jenis_kelamin) ?></td>
						<td><?php echo($pasien->usia)?> tahun</td>
						<td><?php echo($pasien->berat_badan) ?> kg</td>
						<td><?php echo($pasien->tinggi_badan) ?> cm</td>
						<td><?php echo($pasien->tingkat_aktifitas) ?></td>
						<td><?php echo($pasien->alergi) ?></td>
						<td style="display:none"><?php echo($pasien->makanan_ingin) ?></td>
						<td>
							<a href="<?php echo(base_url('genetic/detail/' . $pasien->id_pasien)) ?>" data-toggle="tooltip" data-placement="top" title="detail">
								<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
							</a>
							<a class="btn-delete" href="#" data-toggle="tooltip" data-placement="top" title="delete" data-id="<?php echo($pasien->id_pasien) ?>">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</a>
						</td>
					</tr>
					<?php
					$i++;
					} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  	<div class="modal-dialog modal-sm">
    	<div class="modal-content">
     		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="myModalLabel">Delete</h4>
      		</div>
      		<div class="modal-body">
        		Apakah Anda ingin menghapus data tersebut?
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        		<button type="button" class="btn btn-primary btn-delete-ok" data-id="">Delete</button>
      		</div>
    	</div>
  	</div>
</div>