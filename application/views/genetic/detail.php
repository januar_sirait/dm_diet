<div class="container content">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">
				Menu
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div style="margin-bottom : 10px;">
				<span>Nama Pasien : <?php echo($pasien->nama) ?></span>
				<br/>
				<span>Umur : <?php echo($pasien->usia) ?></span>
			</div>
			<div style="overflow-y: auto;">
				<?php
					$day = array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
					$index = 0;
					foreach ($menu as $menu_sehari) {
						$total_kalori = 0; 
						foreach ($menu_sehari as $makanan) {
							$total_kalori += $makanan->kalori;
						}
				?>

				<div class="schedule-box col-lg-12 col-xs-12">
					<div class="header"><i class="fa fa-calendar"></i> <?php echo($day[$index]) ?></div>
					<div class="body">
						<label>Total Kalori : <?php echo($total_kalori) ?></label>
						<table class="table table-bordered table-header">
							<thead>
								<tr>
									<th>Tipe Makanan</th>
									<th>Sarapan</th>
									<th>Makan Siang</th>
									<th>Makan Malam</th>
								</tr>
							</thead>
							<tbody>
								<?php
								for ($i=0; $i < 5; $i++) { 
									echo('<tr>');
									if ($i == 0) {
										echo('<td>Makanan Pokok</td>');
									}else if($i == 1){
										echo('<td>Lauk Pauk</td>');
									}else if ($i == 2) {
										echo('<td>Sayur</td>');
									}else if ($i == 3) {
										echo('<td>Buah</td>');
									}else if ($i == 4) {
										echo('<td>Pelengkap</td>');
									}

									echo("<td>". $menu_sehari[$i + (0 * 5)]->nama_makanan . " (". $menu_sehari[$i + (0 * 5)]->weight ." g)" ."</td>");
									echo("<td>". $menu_sehari[$i + (1 * 5)]->nama_makanan . " (". $menu_sehari[$i + (1 * 5)]->weight ." g)" ."</td>");
									echo("<td>". $menu_sehari[$i + (2 * 5)]->nama_makanan . " (". $menu_sehari[$i + (2 * 5)]->weight ." g)" ."</td>");
									echo('</tr>');
								} 
								?>
							</tbody>
						</table>
					</div>
				</div>

				<?php
					$index++;
					} 
				?>
			</div>
		</div>
	</div>
</div>