jQuery.fn.extend({
  slideRightShow: function(delay) {
    if (delay == undefined) 
      delay = 0;
    else if(isNaN(delay))
      delay = 0;

    return this.each(function() {
        $(this).delay(delay).show('slide', {direction: 'right'}, 1000);
    });
  },
  slideLeftHide: function(delay) {
    if (delay == undefined) 
      delay = 0;
    else if(isNaN(delay))
      delay = 0;
    return this.each(function() {
      $(this).delay(delay).hide('slide', {direction: 'left'}, 1000);
    });
  },
  slideRightHide: function(delay) {
    if (delay == undefined) 
      delay = 0;
    else if(isNaN(delay))
      delay = 0;
    return this.each(function() {
      $(this).delay(delay).hide('slide', {direction: 'right'}, 1000);
    });
  },
  slideLeftShow: function(delay) {
    if (delay == undefined) 
      delay = 0;
    else if(isNaN(delay))
      delay = 0;
    return this.each(function() {
      $(this).delay(delay).show('slide', {direction: 'left'}, 1000);
    });
  }
});