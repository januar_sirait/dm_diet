jQuery(document).ready(function($) {
	var options = {
		title: 'Perubahan Fitness Terhadap Generasi (Individu awal {populasi})',
        captureRightClick: true,
        animate: false,
        animateReplot: false,
        // seriesColors: ['#579575'],
        seriesDefaults: {
            // renderer: $.jqplot.BarRenderer,
            rendererOptions: {
                smooth: true
            },
            pointLabels: { show: true },
            label : 'Fitness'
        },
        axes: {
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                tickOptions: {
                    angle: -15,
                    fontSize: '9pt'
                },
                label: 'Generasi',
                labelOptions: {
                    fontFamily: 'Helvetica',
                    fontSize: '14pt'
                }
            },
            yaxis: {
                label: 'Fitness',
                labelOptions: {
                    fontFamily: 'Helvetica',
                    fontSize: '14pt'
                },
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickOptions :{
                    formatString: '%#.1f'
                },
                min: 0
            }
        },
        legend : {
	        show: true,
	        location: 'ne',
	        placement: 'insideGrid'
	    }
    };

    var data = [0,0,0,0];

    var plot1 = $.jqplot('placeholder', [data], options);

	$.ajax({
		url: '/genetic/show_grafik',
		type: 'post',
		dataType: 'json',
	})
	.done(function(response) {
		if (plot1) {
			plot1.destroy();
		}

	    options.animate = true;
        options.title = options.title.replace("{populasi}", response.populasi)

	    if (response.data.length >=50) {
            data = [];
            var step = parseInt((response.data.length / 10) / 2);
            var i = 1;
            while(i <= response.data.length)
            {
                data.push([response.data[i][0], response.data[i][1]]);
                i+= 2;
            }
            // for (var i = 1; i <= response.data.length; i++) {
            //     data.push([response.data[i][0], response.data[i][1]]);
            //     i++;
            // };
            plot1 = $.jqplot('placeholder', [data], options);
        }
		else{
            plot1 = $.jqplot('placeholder', [response.data], options);
        };
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

});