jQuery(document).ready(function($) {
	var day = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"];

	changeTableList = function () {
		$('.form-step').each(function(index, el) {
			var item = $($(el).attr('data-list')).parent();
			$(item).removeClass('disabled');
			$(item).removeClass('current');
			$(item).removeClass('done');
			if ($(el).attr('data-current') == "true") {
				$(item).addClass('current');
				return;
			}

			if ($(el).attr('data-finished') == "true") {
				$(item).addClass('done');
				return;
			}
			$(item).addClass('disabled');
		});
	}
	$('div.steps').on('click', 'ul > li > a', function(event) {
		event.preventDefault();
		/* Act on the event */
	});

	$('div.actions').on('click', 'ul > li > a#previous', function(event) {
		event.preventDefault();
		if (!$(this).hasClass('disabled')) {
			if ($("#form-step-1").attr('data-current') == "true") {
			}else if ($("#form-step-2").attr('data-current') == "true"){
				$('#form-step-2').addClass('form-hide');
				$('#form-step-1').removeClass('form-hide');
				$("a#previous").parent().addClass('disabled');

				$("#form-step-2").attr('data-current',"false");
				$("#form-step-1").attr('data-current',"true");
				$('#form-step-1').attr('data-finished', 'false');
			}else if ($("#form-step-3").attr('data-current') == "true"){
				$('#form-step-3').addClass('form-hide');
				$('#form-step-2').removeClass('form-hide');

				$("#form-step-3").attr('data-current',"false");
				$("#form-step-2").attr('data-current',"true");
				$('#form-step-2').attr('data-finished', 'false');

				$('ul > li > a#finish').hide();
				$('ul > li > a#next').show();
			};

			changeTableList();
		};
	});

	$('div.actions').on('click', 'ul > li > a#next', function(event) {
		event.preventDefault();
		/* Act on the event */
		if ($("#form-step-1").attr('data-current') == "true") {
			$('#form-step-1').submit();
		}else if ($("#form-step-2").attr('data-current') == "true"){
			$("#form-step-2").submit();
		};
	});

	$('div.actions').on('click', 'ul > li > a#finish', function(event) {
		event.preventDefault();
		/* Act on the event */
		document.location.href = 'genetic/save';
	});


	FrmStep1 = function () {
		var data = $("#form-step-1").serialize();
		$('#form-step-1 input').attr('disabled','');
	    $('#form-step-1 select').attr('disabled','');
		$.ajax({    
			url: '/genetic/count_weight',
			type: 'POST',
			dataType: 'json',
			data: data,
		})
		.done(function(response) {
			if(response.status){
				$('#form-step-2 label#nama').html(response.data.user.nama);
				$('#form-step-2 label#jenis_kelamin').html(response.data.user.jenis_kelamin);
				$('#form-step-2 label#usia').html(response.data.user.usia);
				$('#form-step-2 label#berat_badan').html(response.data.user.berat_badan);
				$('#form-step-2 label#tinggi_badan').html(response.data.user.tinggi_badan);
				$('#form-step-2 label#tingkat_aktifitas').html(response.data.user.tingkat_aktifitas);
				$('#form-step-2 label#alergi').html(response.data.user.alergi);
				$('#form-step-2 label#makanan_ingin').html(response.data.user.makanan_ingin);

				$('#form-step-2 label#bbi').html(response.data.bbi);
				$('#form-step-2 label#kb').html(response.data.kb);
				$('#form-step-2 label#kfa').html(response.data.kfa);
				$('#form-step-2 label#kfu').html(response.data.kfu);
				$('#form-step-2 label#imt').html(response.data.imt);
				$('#form-step-2 label#total_kalori').html(response.data.total_kalori);

				var jenis_diet = response.data.jenis_diet;
				var row =$("<tr class='item'></tr>");
				row.append("<td>"+ jenis_diet.jenis_diet +"</td>");
				row.append("<td>"+ jenis_diet.energi +"</td>");
				row.append("<td>"+ jenis_diet.protein +"</td>");
				row.append("<td>"+ jenis_diet.lemak +"</td>");
				row.append("<td>"+ jenis_diet.karbohidrat +"</td>");
				row.append("<td>"+ jenis_diet.kolesterol +"</td>");
				row.append("<td>"+ jenis_diet.serat +"</td>");
				row.append("<td>"+ jenis_diet.natrium +"</td>");
				row.append("<td>"+ jenis_diet.sukrosa +"</td>");
				$('tr.item').remove();
				$('#table-kalori').append(row);

				$('#form-step-1').addClass('form-hide');
				$('#form-step-2').removeClass('form-hide');
				$("a#previous").parent().removeClass('disabled');

				$("#form-step-1").attr('data-current',"false");
				$("#form-step-2").attr('data-current', "true");

				$('#form-step-1').attr('data-finished', 'true');
				changeTableList();
	        }
        })
		.fail(function(err) {
			alert(err);
		})
		.always(function() {
			$('#form-step-1 input').removeAttr('disabled');
			$('#form-step-1 select').removeAttr('disabled');
		});
	}

	FrmStep2 = function(){
		var data = $("#form-step-2").serialize();
		$('#form-step-2 input').attr('disabled','');
	    $('#form-step-2 select').attr('disabled','');
	    $('div.loading img').css('display','table-cell');
		$.ajax({    
			url: '/genetic/process',
			type: 'POST',
			dataType: 'json',
			data: data,
		})
		.done(function(response) {
			if(response.status){
				$('#schedule .schedule-box').remove();

				var diet = response.data;
				var hari = 0;
				$.each(diet, function(index, val) {
					var block = createScheduleBlock(day[hari], val.total_kalori, val.menu)
					$('#schedule').append(block);
					hari++;
				});

				$('#form-step-2').addClass('form-hide');
				$('#form-step-3').removeClass('form-hide');
				$("a#previous").parent().removeClass('disabled');

				$("#form-step-2").attr('data-current',"false");
				$("#form-step-3").attr('data-current', "true");

				$('#form-step-2').attr('data-finished', 'true');
				changeTableList();
				$('ul > li > a#next').hide();
				$('ul > li > a#finish').show();
	        }
        })
		.fail(function(err) {
			alert(err);
		})
		.always(function() {
			$('#form-step-2 input').removeAttr('disabled');
			$('#form-step-2 select').removeAttr('disabled');
			$('div.loading img').css('display','none');
		});
	}

	$('#form-step-1').myvalidate(FrmStep1);
	$('#form-step-2').myvalidate(FrmStep2);

	createScheduleBlock = function(day, total_kalori, data){
		var blockString = 	'<div class="schedule-box col-lg-12 col-xs-12">'+
								'<div class="header"><i class="fa fa-calendar"></i> {day}</div>'+
								'<div class="body">'+
									'<label>Total Kalori : {kalori}</label>'+
									'<table class="table table-bordered table-header">'+
										'<thead>'+
										'<tr>'+
											'<th>Tipe Makanan</th>'+
											'<th>Sarapan</th>'+
											'<th>Makan Siang</th>'+
											'<th>Makan Malam</th>'+
										'</tr>'+
										'</thead>'+
									'</table>'+
								'</div>'+
							'</div>';
		blockString = blockString.replace('{day}', day);
		blockString = blockString.replace('{kalori}', total_kalori);
 
		var block = $(blockString);
		var table = block.find('.table');

		for (var i = 0; i < 5; i++) {
			var row = $('<tr></tr>');
			if (i == 0) {
				row.append('<td>Makanan Pokok</td>');
			}else if(i == 1){
				row.append('<td>Lauk Pauk</td>');
			}else if (i == 2) {
				row.append('<td>Sayur</td>');
			}else if (i == 3) {
				row.append('<td>Buah</td>');
			}else if (i == 4) {
				row.append('<td>Pelengkap</td>');
			}

			row.append("<td>"+ data[i + (0 * 5)].nama_makanan + " ("+ data[i + (0 * 5)].weight +" g)" +"</td>");
			row.append("<td>"+ data[i + (1 * 5)].nama_makanan + " ("+ data[i + (1 * 5)].weight +" g)" +"</td>");
			row.append("<td>"+ data[i + (2 * 5)].nama_makanan + " ("+ data[i + (2 * 5)].weight +" g)" +"</td>");

			table.append(row);
		};

		return block;
	}
});