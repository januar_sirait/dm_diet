/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $.fn.myvalidate = function (submitHandler) {
        $(this).validate({
            errorClass: 'has-error',
            validClass: 'has-success',
            success: function(label) {
                var placement = $(label).parent();
                placement.removeClass('has-error').addClass('has-success');
                label.remove();
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass('input-group')) {
                    var placement = $(element).parent().parent().parent();
                    placement.addClass('has-error');
                    element.parent().after(error);
                } else {
                    var placement = $(element).parent().parent();
                    placement.addClass('has-error');
                    element.after(error);
                }
            },
            highlight: function(element, errorClass, validClass) {
                if ($(element).parent().hasClass('input-group')) {
                    var placement = $(element).parent().parent().parent();
                    placement.addClass(errorClass).removeClass(validClass);
                } else {
                    var placement = $(element).parent().parent();
                    placement.addClass(errorClass).removeClass(validClass);
                }
            },
            unhighlight: function(element, errorClass, validClass) {
                if ($(element).parent().hasClass('input-group')) {
                    var placement = $(element).parent().parent().parent();
                    placement.removeClass(errorClass).addClass(validClass);
                    placement.find('label.has-error').remove();
                } else {
                    var placement = $(element).parent().parent();
                    placement.removeClass(errorClass).addClass(validClass);
                    placement.find('label.has-error').remove();
                }
            },
            submitHandler: function(form){
	        submitHandler();
	    }
        });
        return this;
    };
}(jQuery))